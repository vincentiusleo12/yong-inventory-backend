<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMetresToWarehouseproducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            ALTER TABLE warehouseproducts ADD COLUMN metres INT UNSIGNED NOT NULL DEFAULT 0 AFTER quantity;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("
            ALTER TABLE warehouseproducts DROP COLUMN metres;
        ");
    }
}
