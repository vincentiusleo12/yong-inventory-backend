<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'jwt.auth'], function () {
    Route::post('auth/logout', 'AuthController@logout');
    Route::get('logs/get', 'LogController@get');
    Route::post('products/create', 'ProductController@create');
    Route::post('products/update', 'ProductController@update');
    Route::post('products/destroy', 'ProductController@destroy');
    Route::post('products/scan', 'ProductController@scan');
    Route::get('products', 'ProductController@get');
    Route::get('warehouseproducts/get', 'WarehouseController@getProducts');
    Route::get('warehouse/get', 'WarehouseController@get');
    Route::get('auth/check','AuthController@checkToken');
    Route::get('auth/user','AuthController@getUser');
    Route::post('user/change-password','UserController@update');
});

Route::post('auth/login','AuthController@login');
Route::post('auth/register','AuthController@register');