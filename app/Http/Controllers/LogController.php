<?php

namespace App\Http\Controllers;

use App\Models\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LogController extends Controller
{
    public function __construct()
    {
        $this->logModel = new Log();
    }

    public function get(Request $request){
        $rules = [
            'paginate' => 'nullable|integer|min:1'
        ];
        $customMessages = [
        ];
        $customAttributes = [
        ];
        $request->validate($rules, $customMessages, $customAttributes);

        $user = Auth::user();

        $logsData = $this->logModel->with('user')->orderBy('id', 'DESC');

        if(isset($request->paginate)){
            $logsData = $logsData->paginate($request->paginate);
        }else{
            $logsData = $logsData->get();        
        }

        $data = [
            'logs' => $logsData
        ];

        $response = [
            'api_status' => true,
            'message' => "Success",
            'data' => $data
        ];

        return response()->json($response, 200);
    }

}
