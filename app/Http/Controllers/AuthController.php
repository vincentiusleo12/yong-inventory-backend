<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Log;
use App\Models\User;
use DB, JWTAuth, JWTException, Mail, Validator;
use Illuminate\Support\Str;
use Carbon\Carbon;

class AuthController extends Controller
{
	/**
	 * Create a new AuthController instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->logModel = new Log();
		$this->userModel = new User();
	}

	/**
	 * Get a JWT via given credentials.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function login(Request $request){
		$rules = [
			'email' => 'required|email',
			'password' => 'required|string|min:6',
		];
		$customMessages = [
		];
		$customAttributes = [
		];
		$request->validate($rules, $customMessages, $customAttributes);

		$data = [
			'email' => $request->email,
			'password' => $request->password
		];
		
		
		try {
			// attempt to verify the credentials and create a token for the user
			if (! $token = JWTAuth::attempt($data)) {
				return response()->json(['api_status' => false, 'error' => 'Email atau password Anda salah.'], 404);
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json(['api_status' => false, 'error' => 'Login gagal, silahkan coba lagi.'], 500);
		}

		$user = Auth::user();

		return $this->createNewToken($token);
	}

	/**
	 * Register a User.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function register(Request $request) {
		$rules = [
			'name' => 'required|string|between:2,100',
			'email' => 'required|string|email|max:100|unique:users',
			'password' => 'required|string|min:6',
			'confirm_password' => 'required|string|min:6'
		];
		$customMessages = [
		];
		$customAttributes = [
		];
		$request->validate($rules, $customMessages, $customAttributes);

		if($request->password != $request->confirm_password){
			$response = [
				'api_status' => false,
				'message' => "Password tidak cocok dengan konfirmasi password. Silahkan masukkan kembali password Anda.",
				'data' => null
			];

			return response()->json($response, 500);
		}

		$checkEmail = $this->userModel->where('email', 'LIKE', $request->email)->first();

		if($checkEmail){
			$response = [
				'api_status' => false,
				'message' => "Email yang Anda masukkan sudah terdaftar.",
				'data' => null
			];

			return response()->json($response, 500);
		}

		$data = [
			'name' => $request->name,
			'email' => $request->email,
			'password' => bcrypt($request->password)
		];

		$user = $this->userModel->create($data);

		return response()->json([
			'api_status' => true,
			'message' => 'Anda telah berhasil mendaftar.',
			'user' => $user
		], 201);
	}


	/**
	 * Log the user out (Invalidate the token).
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function logout(Request $request) {
		$rules = [
			'token' => 'required|string',
		];
		$customMessages = [
		];
		$customAttributes = [
		];
		$request->validate($rules, $customMessages, $customAttributes);		
		try {
			JWTAuth::invalidate($request->token);
			return response()->json(['api_status' => true, 'message'=> "Anda telah keluar."]);
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json(['api_status' => false, 'error' => 'Gagal untuk keluar, silahkan coba lagi.'], 500);
		}
	}

	/**
	 * Refresh a token.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function refresh() {
		return $this->createNewToken(auth()->refresh());
	}

	/**
	 * Get the token array structure.
	 *
	 * @param  string $token
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function createNewToken($token){
		return response()->json([
			'api_status' => true,
			'access_token' => $token,
			'token_type' => 'bearer',
			'expires_in' => null,
			'user' => Auth::user()
		]);
	}

	public function checkToken(){
		return response()->json([ 'valid' => Auth::check(), 'user' => Auth::user() ]);
	}

	public function getUser(){
		return response()->json([ 'user' => Auth::user() ]);
	}

	public function requestForgetPassword(Request $request){
		$rules = [
			'email' => 'required|string|email|exists:users,email',
		];
		$customMessages = [
		];
		$customAttributes = [
		];
		$request->validate($rules, $customMessages, $customAttributes);

		$email = $request->email;

		$getUser = null;
		if(filter_var($email, FILTER_VALIDATE_EMAIL))
			$getUser = $this->userModel->where('email', 'LIKE', $email)->first();

		//validate email not exists
		if($getUser == null){
			$response = [
				'api_status' => false,
				'message' => "$email tidak ditemukan.",
				'data' => null
			];
			return response()->json($response, 500);
		}

		$data = [
			"name" => $getUser['name'],
			"resetpasswordlink" => env("WEB_URL")."/create-new-password?t=".hash('sha256', $getUser['id'])
		];

		$subject = "Permohonan Reset Password - ".env("APP_NAME");

		$body = "
		Halo ".$data["name"].",
		<br/>
		<br/>
		Silahkan klik tombol di bawah ini untuk mengubah password Anda:<br/><br/>
		<a href='".$data['resetpasswordlink']."' class='button'><div style='margin-left:auto; margin-right:auto;'>Ubah Password</div></a>
		<br/>
		<br/>
		Jika Anda tidak meminta merubah password, abaikan email ini.
		<br/>
		<br/>
		Jika ada permasalahan dalam menekan tombol di atas, silahkan <i>copy</i> tautan di bawah ini dan masukkan pada <i>browser</i> Anda:<br/><br/>
		<a href='".$data["resetpasswordlink"]."'>".$data["resetpasswordlink"]."</a>
		";

		Mail::send('email.message', ['body' => $body], function($mail) use ($subject, $email, $data){
			$mail->from(env('MAIL_FROM_ADDRESS'), "PMI Surabaya");
			$mail->to($email, $data["name"]);
			$mail->subject($subject);
		});

		$blurEmail = "";
		$startingPoint = strpos($getUser['email'], '@'); // get @ index in email
		$getEndEmail = substr($getUser['email'], $startingPoint); // last email from @ (@mail.com)
		$getStartEmail = substr($getUser['email'], 0 , 4); // first 4 letter in email (skyb)
		$totalHide = strlen($getUser['email']) - 4 - strlen($getEndEmail);
		if($totalHide <= 4){
			$blurEmail = "****";
		}else{
			for($i = 0 ; $i < $totalHide ; $i++){
				$blurEmail = $blurEmail."*";
			}
		}
		$hiddenEmail = $getStartEmail.$blurEmail.$getEndEmail;

		$message = "Email berisi langkah selanjutnya untuk me-reset password Anda telah dikirimkan pada ".$hiddenEmail.".";
		$response = [
			'api_status' => true,
			'message' => $message,
			'data' => null
		];

		return response()->json($response, 200);
	}

	public function setNewPassword(Request $request){
		$rules = [
			'token' => 'required|string',
			'new_password' => 'required|string|min:6',
			'confirm_password' => 'required|string|min:6'
		];
		$customMessages = [
		];
		$customAttributes = [
		];
		$request->validate($rules, $customMessages, $customAttributes);

		if($request->new_password != $request->confirm_password){
			$response = [
				'api_status' => false,
				'message' => "Password tidak cocok dengan konfirmasi password, silahkan masukkan kembali password Anda.",
				'data' => null
			];
			return response()->json($response, 500);
		}

		$token = $request->token;
		$user = $this->userModel->where(DB::raw('SHA2(id, 256)'), $token)->first();

		if($user != null){
			$data['password'] = bcrypt($request->new_password);
			$userUpdated = $this->userModel->where('id', $user->id)->update($data);
			if($userUpdated){
				$response = [
					'api_status' => true,
					'message' => "Password Anda berhasil diubah. Silahkan login kembali.",
					'data' => null
				];

				return response()->json($response, 200);
			}else{
				$response = [
					'api_status' => false,
					'message' => "Gagal mengubah password, silahkan coba lagi.",
					'data' => null
				];
				return response()->json($response, 500);
			}
			
		}else{
			$response = [
				'api_status' => false,
				'message' => "Token tidak valid, silahkan melakukan permohonan lupa password kembali.",
				'data' => null
			];
			return response()->json($response, 500);
		}
	}
}