<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Log;
use App\Models\User;
use Carbon\Carbon;
use JWTAuth;

class UserController extends Controller
{
	//
	public function __construct()
	{
		$this->logModel = new Log();
		$this->userModel = new User();

	}

	public function getDetailById(Request $request){
		$rules = [
			'id' => 'nullable|integer|exists:users,id'
		];
		$customMessages = [
		];
		$customAttributes = [
		];
		$request->validate($rules, $customMessages, $customAttributes);

		$data = [
			'user' => isset($request->id) ? $this->userModel->find($request->id) : Auth::user(),
		];

		$response = [
			'api_status' => true,
			'message' => "Success",
			'data' => $data
		];

		return response()->json($response, 200);
	}

	public function update(Request $request){
		$rules = [
			'id' => 'nullable|integer|exists:users,id',
			'name' => 'nullable|string',
			'old_password' => 'string|required',
			'new_password'=> 'required|string', 
			'confirm_password' => 'nullable|string|required_with:password',	
		];
		$customMessages = [
		];
		$customAttributes = [
		];
		$request->validate($rules, $customMessages, $customAttributes);

		$data = [];

		
		$user = Auth::user();

		$check = [
			'email' => $user->email,
			'password'=> $request->old_password
		]; 
		
		
		if(isset($request->new_password)){
			if($request->new_password != $request->confirm_password){
				$response = [
					'api_status' => false,
					'message' => "Password tidak cocok. Silahkan masukkan kembali password Anda.",
					'data' => null
				];

				return response()->json($response, 200);
			}
			if (! $token = JWTAuth::attempt($check)) {
				return response()->json(['api_status' => false, 'message' => 'Password lama salah.'], 200);
			}
			// if($user->password != bcrypt($request->old_password)){
			// 	$response = [
			// 		'api_status' => false,
			// 		'message' => "Password lama salah. Silahkan masukkan kembali password Anda.",
			// 		'data' => null
			// 	];

			// 	return response()->json($response, 200);
			// }
			
			if($request->old_password == $request->new_password){
				$response = [
					'api_status' => false,
					'message' => "Password baru tidak boleh sama dengan password lama.",
					'data' => null
				];

				return response()->json($response, 200);
			}

			$data['password'] = bcrypt($request->new_password);
		}

		$updatedData = $this->userModel->where('id', $user->id)->update($data);

		if($updatedData){
			$response = [
				'api_status' => true,
				'message' => "Password berhasil dirubah.",
				'data' => null
			];

			return response()->json($response, 200);
		}else{
			$response = [
				'api_status' => false,
				'message' => "Gagal mengubah Password.",
				'data' => null
			];

			return response()->json($response, 500);
		}
	}
}
