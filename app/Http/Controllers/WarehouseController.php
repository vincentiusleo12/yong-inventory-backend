<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Warehouseproduct;
use App\Models\Warehouse;
use Illuminate\Support\Facades\Auth;

class WarehouseController extends Controller
{
    public function __construct()
    {
        $this->productModel = new Product();
        $this->warehouse = new Warehouse();
        $this->warehouseProductModel = new Warehouseproduct();
    }
    public function getProducts(Request $request){
        $rules = [
            'paginate' => 'nullable|integer|min:1',
            'warehouse_id' => 'nullable|integer'
        ];
        $customMessages = [
        ];
        $customAttributes = [
        ];
        $request->validate($rules, $customMessages, $customAttributes);

        $user = Auth::user();

        $warehouseData = $this->warehouseProductModel->with('product');

        $warehouseData = $warehouseData->where('warehouse_id',$request->warehouse_id)->get();
       

        $data = [
            'products' => $warehouseData
        ];

        $response = [
            'api_status' => true,
            'message' => "Success",
            'data' => $data
        ];

        return response()->json($response, 200);
    }
    public function get(Request $request){
        $rules = [
            'paginate' => 'nullable|integer|min:1',
            'warehouse_id' => 'nullable|integer'
        ];
        $customMessages = [
        ];
        $customAttributes = [
        ];
        $request->validate($rules, $customMessages, $customAttributes);

        $user = Auth::user();
        $warehouseData = $this->warehouse->get();
        $data = [
            'warehouse' => $warehouseData
        ];

        $response = [
            'api_status' => true,
            'message' => "Success",
            'data' => $data
        ];

        return response()->json($response, 200);
    }
}
