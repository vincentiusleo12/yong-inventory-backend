<?php

namespace App\Http\Controllers;

use App\Models\Log;
use App\Models\Product;
use App\Models\Warehouseproduct;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ProductController extends Controller
{
    public function __construct()
    {	
    	$this->logModel = new Log();
        $this->productModel = new Product();
        $this->warehouseProductModel = new Warehouseproduct();
        $this->warehouseModel = new Warehouse();
    }

    public function get(Request $request){
        $rules = [
            'paginate' => 'nullable|integer|min:1',
            'id' => 'nullable|integer'
        ];
        $customMessages = [
        ];
        $customAttributes = [
        ];
        $request->validate($rules, $customMessages, $customAttributes);


        $productsData = $this->productModel->with('warehouseproducts');

        
        if(isset($request->id)){
            $productsData = $productsData->where('id',$request->id)->first();
        }
        else{
            if(isset($request->paginate)){
                $productsData = $productsData->paginate($request->paginate);
            }
            else{
                $productsData = $productsData->get();        
            }
        }

        $data = [
            'products' => $productsData
        ];

        $response = [
            'api_status' => true,
            'message' => "Success",
            'data' => $data
        ];

        return response()->json($response, 200);
    }

    public function create(Request $request){
		$rules = [
			'name' => 'required|string',
			'sku' => 'string|nullable',
			'price' => 'required|integer',
            'qty_1' => 'required|integer',
            'qty_2' => 'required|integer',
            'qty_3' => 'required|integer',
            'metre_1' => 'required|integer',
            'metre_2' => 'required|integer',
            'metre_3' => 'required|integer',
		];
		$customMessages = [
		];
		$customAttributes = [
		];
		$request->validate($rules, $customMessages, $customAttributes);

		$user = Auth::user();
		if($user->isadmin != 1){
			$response = [
				'api_status' => false,
				'message' => "Anda tidak punya akses untuk melakukan aksi ini.",
				'data' => null
			];

			return response()->json($response, 403);
		}

		if(isset($request->name)){
			$checkName = $this->productModel->where('name', 'LIKE', $request->name)->where('id', '!=', $request->id)->first();

			if($checkName){
				$response = [
					'api_status' => false,
					'message' => "Nama yang Anda masukkan sudah terdaftar.",
					'data' => null
				];

				return response()->json($response, 200);
			}
		}

		if(isset($request->sku)){
			$checkSKU = $this->productModel->where('sku', 'LIKE', $request->sku)->where('id', '!=', $request->id)->first();

			if($checkSKU){
				$response = [
					'api_status' => false,
					'message' => "SKU yang Anda masukkan sudah terdaftar.",
					'data' => null
				];

				return response()->json($response, 200);
			}
		}

		$productData = [
			'name' => $request->name,
			'sku' => $request->sku,
			'price' => $request->price
		];

		$createdProduct = $this->productModel->create($productData);

        $productWarehouseData1 = [
			'warehouse_id' => 1,
            'product_id' => $createdProduct->id,
			'quantity' => $request->qty_1,
			'metres' => $request->metre_1,
		];
        $productWarehouseData2 = [
			'warehouse_id' => 2,
            'product_id' => $createdProduct->id,
			'quantity' => $request->qty_2,
			'metres' => $request->metre_2,
		];
        $productWarehouseData3 = [
			'warehouse_id' => 3,
            'product_id' => $createdProduct->id,
			'quantity' => $request->qty_3,
			'metres' => $request->metre_3,
		];

        $createdWarehouseProduct1 = $this->warehouseProductModel->create($productWarehouseData1);
        $createdWarehouseProduct2 = $this->warehouseProductModel->create($productWarehouseData2);
        $createdWarehouseProduct3 = $this->warehouseProductModel->create($productWarehouseData3);

		if($createdProduct){
			$logData = [
				'user_id' => Auth::user()->id,
				'description' => 'menambahkan data produk '.$createdProduct->name.'.'
			];
			$createdLog = $this->logModel->create($logData);
			$response = [
				'api_status' => true,
				'message' => "Produk berhasil ditambahkan.",
				'data' => null
			];

			return response()->json($response, 200);
		}else{
			$response = [
				'api_status' => false,
				'message' => "Produk gagal ditambahkan.",
				'data' => null
			];

			return response()->json($response, 500);
		}
	}

    public function update(Request $request){
		$rules = [
            'id' => 'required|integer|exists:products,id',
			'name' => 'required|string',
			'sku' => 'string|nullable',
			'price' => 'required|integer',
            'qty_1' => 'required|integer',
            'qty_2' => 'required|integer',
            'qty_3' => 'required|integer',
            'metre_1' => 'required|integer',
            'metre_2' => 'required|integer',
            'metre_3' => 'required|integer',
		];
		$customMessages = [
		];
		$customAttributes = [
		];
		$request->validate($rules, $customMessages, $customAttributes);

		$user = Auth::user();
		if($user->isadmin != 1){
			$response = [
				'api_status' => false,
				'message' => "Anda tidak punya akses untuk melakukan aksi ini.",
				'data' => null
			];

			return response()->json($response, 403);
		}

		$productData = [];

		if(isset($request->name)){
			$checkName = $this->productModel->where('name', 'LIKE', $request->name)->where('id', '!=', $request->id)->first();

			if($checkName){
				$response = [
					'api_status' => false,
					'message' => "Nama yang Anda masukkan sudah terdaftar.",
					'data' => null
				];

				return response()->json($response, 200);
			}

			$productData['name'] = $request->name;
		}

		if(isset($request->sku)){
			$checkSKU = $this->productModel->where('sku', 'LIKE', $request->sku)->where('id', '!=', $request->id)->first();

			if($checkSKU){
				$response = [
					'api_status' => false,
					'message' => "SKU yang Anda masukkan sudah terdaftar.",
					'data' => null
				];

				return response()->json($response, 200);
			}

			$productData['sku'] = $request->sku;
		}

		if(isset($request->price)){
			$productData['price'] = $request->price;
		}

        if(isset($request->qty_1)){
            $updatedQuantity = ['quantity' => $request->qty_1];
			$this->warehouseProductModel->where('warehouse_id','1')->where('product_id',$request->id)->update($updatedQuantity);
		}

		if(isset($request->qty_2)){
			$updatedQuantity = ['quantity' => $request->qty_2];
			$this->warehouseProductModel->where('warehouse_id','2')->where('product_id',$request->id)->update($updatedQuantity);
		}

        if(isset($request->qty_3)){
			$updatedQuantity = ['quantity' => $request->qty_3];
			$this->warehouseProductModel->where('warehouse_id','3')->where('product_id',$request->id)->update($updatedQuantity);
		}

		if(isset($request->metre_1)){
            $updatedMetre = ['metres' => $request->metre_1];
			$this->warehouseProductModel->where('warehouse_id','1')->where('product_id',$request->id)->update($updatedMetre);
		}

		if(isset($request->metre_2)){
			$updatedMetre = ['metres' => $request->metre_2];
			$this->warehouseProductModel->where('warehouse_id','2')->where('product_id',$request->id)->update($updatedMetre);
		}

        if(isset($request->metre_3)){
			$updatedMetre = ['metres' => $request->metre_3];
			$this->warehouseProductModel->where('warehouse_id','3')->where('product_id',$request->id)->update($updatedMetre);
		}

		$updatedProduct = $this->productModel->where('id', $request->id)->update($productData);

		if($updatedProduct){
			$productData = $this->productModel->where('id', $request->id)->first();
			$logData = [
				'user_id' => Auth::user()->id,
				'description' => 'mengubah data produk '.$productData->name.'.'
			];
			$createdLog = $this->logModel->create($logData);
			$response = [
				'api_status' => true,
				'message' => "Produk berhasil diedit.",
				'data' =>  $this->productModel->with('warehouseproducts')->where('id', $request->id)->first()
			];

			return response()->json($response, 200);
		}else{
			$response = [
				'api_status' => false,
				'message' => "Produk gagal diupdate.",
				'data' => null
			];

			return response()->json($response, 500);
		}
	}

    public function destroy(Request $request){
		$rules = [
			'id' => 'required|integer|exists:products,id',
		];
		$customMessages = [
		];
		$customAttributes = [
		];
		$request->validate($rules, $customMessages, $customAttributes);

		$user = Auth::user();
		if($user->isadmin != 1){
			$response = [
				'api_status' => false,
				'message' => "Anda tidak punya akses untuk melakukan aksi ini.",
				'data' => null
			];

			return response()->json($response, 403);
		}
		$productData = $this->productModel->where('id', $request->id)->first();
		$deletedProduct = $this->productModel->where('id', $request->id)->delete();
		if($deletedProduct){
			$deletedWarehouseProduct = $this->warehouseProductModel->where('product_id', $request->id)->delete();
			$logData = [
				'user_id' => Auth::user()->id,
				'description' => 'menghapus data produk '.$productData->name.'.'
			];
			$createdLog = $this->logModel->create($logData);
			$response = [
				'api_status' => true,
				'message' => "Produk berhasil dihapus.",
				'data' => null
			];

			return response()->json($response, 200);
		}else{
			$response = [
				'api_status' => false,
				'message' => "Produk gagal dihapus.",
				'data' => null
			];

			return response()->json($response, 500);
		}
	}

	public function scan(Request $request){
		$rules = [
            'sku' => 'required|string',
            'warehouse_id' => 'required|integer|exists:warehouses,id',
			'action' => 'required|string|in:in,out',
			'quantity' => 'required|integer',
			'metres' => 'required|integer'
		];
		$customMessages = [
		];
		$customAttributes = [
		];
		$request->validate($rules, $customMessages, $customAttributes);

		// $user = Auth::user();
		// if($user->department_id != 1 && $user->department_id != 4){
		// 	$response = [
		// 		'api_status' => false,
		// 		'message' => "Anda tidak punya akses untuk melakukan aksi ini.",
		// 		'data' => null
		// 	];

		// 	return response()->json($response, 403);
		// }

		$productData = $this->productModel->where(function($q) use ($request){
			$q->where('sku', 'LIKE', $request->sku)->orWhere('name', 'LIKE', $request->sku);
		})->first();
		if(!isset($productData)){
			$response = [
				'api_status' => false,
				'message' => "Produk tidak ditemukan.",
				'data' => null
			];

			return response()->json($response, 200);
		}

		$warehouseProduct = $this->warehouseProductModel->where('warehouse_id', $request->warehouse_id)->where('product_id', $productData->id)->first();
		if(!isset($warehouseProduct)){
			$response = [
				'api_status' => false,
				'message' => "Data produk di gudang tidak ditemukan.",
				'data' => null
			];

			return response()->json($response, 200);
		}
		if($request->action == 'in'){
			$warehouseProduct->quantity += isset($request->quantity) ? $request->quantity : 1;
			$warehouseProduct->metres += isset($request->metres) ? $request->metres : 0;
		}else if($request->action == 'out'){
			$warehouseProduct->quantity -= isset($request->quantity) ? $request->quantity : 1;
			$warehouseProduct->metres -= isset($request->metres) ? $request->metres : 0;
		}
		if($warehouseProduct->quantity < 0){
			$response = [
				'api_status' => false,
				'message' => "Jumlah tidak bisa negatif.",
				'data' => null
			];

			return response()->json($response, 200);
		}

		if($warehouseProduct->metres < 0){
			$response = [
				'api_status' => false,
				'message' => "Jumlah meter tidak bisa negatif.",
				'data' => null
			];

			return response()->json($response, 200);
		}
		$result = $warehouseProduct->save();

		if($result){
			// $logData = [
			// 	'user_id' => Auth::user()->id,
			// 	'description' => 'mengubah data dokter '.$doctorData->name.'.'
			// ];
			// $createdLog = $this->logModel->create($logData);
			$warehouseData = $this->warehouseModel->where('id', $request->warehouse_id)->first();
			if($request->action == 'in'){
				$logData = [
					'user_id' => Auth::user()->id,
					'description' => 'memasukkan '.$productData->name.' di '.$warehouseData->name.' sebanyak '.$request->quantity.' buah ('.$request->metres.'meter).'
				];
			}else{
				$logData = [
					'user_id' => Auth::user()->id,
					'description' => 'mengeluarkan '.$productData->name.' di '.$warehouseData->name.' sebanyak '.$request->quantity.' buah ('.$request->metres.'meter).'
				];
			}
			$warehouseProductData = $this->warehouseProductModel->with('product')->where('warehouse_id', $request->warehouse_id)->where('product_id', $productData->id)->first();

			$warehouseProductData->quantity = $request->quantity;
			$warehouseProductData->metres = $request->metres;

			$createdLog = $this->logModel->create($logData);
			$response = [
				'api_status' => true,
				'message' => "Stok berhasil diupdate.",
				'data' => $warehouseProductData
			];

			return response()->json($response, 200);
		}else{
			$response = [
				'api_status' => false,
				'message' => "Stok gagal diupdate.",
				'data' => null
			];

			return response()->json($response, 500);
		}
	}
}
