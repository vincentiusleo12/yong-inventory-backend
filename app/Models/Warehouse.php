<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'address', 'created_at', 'updated_at', 'deleted_at'];
	protected $guarded = ['id'];
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	public function warehouseproducts(){
		return $this->hasMany('App\Models\Warehouseproduct', 'warehouse_id');
    }
}
