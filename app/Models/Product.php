<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'sku', 'price', 'created_at', 'updated_at', 'deleted_at'];
	protected $guarded = ['id'];
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	public function warehouseproducts(){
		return $this->hasMany('App\Models\Warehouseproduct', 'product_id');
    }
}
