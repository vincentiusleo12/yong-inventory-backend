<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouseproduct extends Model
{
    protected $fillable = ['warehouse_id', 'product_id', 'metres', 'quantity', 'created_at', 'updated_at'];
    protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at'];

    public function warehouse(){
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_id');
    }

    public function product(){
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}
