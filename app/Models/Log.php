<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
	protected $fillable = ['user_id', 'description', 'created_at', 'updated_at'];
	protected $guarded = ['id'];
	protected $dates = ['created_at', 'updated_at'];

	public function user(){
		return $this->belongsTo('App\Models\User', 'user_id');
	}

}
